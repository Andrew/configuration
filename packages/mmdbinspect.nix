{ buildGoModule
, fetchFromGitHub
, lib
, unstableGitUpdater
}:

buildGoModule {
  pname = "mmdbinspect";
  version = "unstable-2023-12-13";

  src = fetchFromGitHub {
    owner = "maxmind";
    repo = "mmdbinspect";
    rev = "6101ed839d9d132498fed30b1c30866a7fad6e53";
    hash = "sha256-z45hBvnisEAlnGTIkK2vzn+dtiFjcyYWhmAIAMom3L4=";
    fetchSubmodules = true;
  };

  vendorHash = "sha256-HNgofsfMsqXttnrNDIPgLHag+2hqQTREomcesWldpMo=";

  passthru.updateScript = unstableGitUpdater { };

  meta = {
    homepage = "https://github.com/maxmind/mmdbinspect";
    license = with lib.licenses; [ asl20 mit ];
  };
}
