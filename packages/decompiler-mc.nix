{ fetchFromGitHub
, lib
, makeWrapper
, stdenv
, unstableGitUpdater

  # Dependencies
, jre
, python3
}:

stdenv.mkDerivation rec {
  pname = "decompiler-mc";
  version = "unstable-2022-06-21";

  src = fetchFromGitHub {
    owner = "hube12";
    repo = "DecompilerMC";
    rev = "a905ea1fa818df1ebfd6a1e0d7c61658737ed1bc";
    hash = "sha256-0ZOgXG/ShACJz5NznMD4jxt8nb5XVyJBfcUZoSFsKAI=";
  };

  buildInputs = [ makeWrapper python3 ];

  installPhase = ''
    mkdir --parents $out
    cp --recursive $src/lib $out/lib

    install -D main.py $out/bin/${pname}
    substituteInPlace $out/bin/${pname} --replace ./lib $out/lib
    wrapProgram $out/bin/${pname} --prefix PATH : ${lib.makeBinPath [ jre ]}
  '';

  passthru.updateScript = unstableGitUpdater { };

  meta = {
    description = "Automated decompilation of Minecraft";
    homepage = "https://github.com/hube12/DecompilerMC";
    license = with lib.licenses; [
      asl20 # Fernflower (vendored)
      bsd3 # SpecialSource (vendored)
      mit # DecompilerMC, CFR (vendored)
    ];
    mainProgram = pname;
  };
}
