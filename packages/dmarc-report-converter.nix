{ buildGoModule
, fetchFromGitHub
, lib
, unstableGitUpdater
}:

buildGoModule {
  pname = "dmarc-report-converter";
  version = "unstable-2023-03-01";

  src = fetchFromGitHub {
    owner = "tierpod";
    repo = "dmarc-report-converter";
    rev = "1f6e13e53d0e13c77031b3d3ee20fdd76b7d9f32";
    hash = "sha256-rqh4MCpO226HBKltVNYDpxkWqQh2FYKvElhoNIbQDWI=";
  };

  vendorHash = null;

  passthru.updateScript = unstableGitUpdater { };

  meta = {
    homepage = "https://github.com/tierpod/dmarc-report-converter";
    license = lib.licenses.mit;
  };
}
