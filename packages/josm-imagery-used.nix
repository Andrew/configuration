{ buildJosmPlugin
, fetchFromGitHub
, gitUpdater
, lib
}:

buildJosmPlugin rec {
  pname = "josm-imagery-used";
  version = "0.0.1";

  src = fetchFromGitHub {
    owner = "AndrewKvalheim";
    repo = "imagery_used";
    rev = "v${version}";
    hash = "sha256-JGYKN9ggUom48StabPoswoYymJiMo7tpXv/t2Ithurg=";
  };

  pluginName = "imagery_used";

  passthru.updateScript = gitUpdater { rev-prefix = "v"; };

  meta = {
    description = "JOSM plugin to populate imagery_used in changesets";
    homepage = "https://github.com/AndrewKvalheim/imagery_used";
    license = lib.licenses.gpl3;
  };
}
