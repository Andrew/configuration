{ pkgs, ... }:

let
  inherit (builtins) readFile;

  identity = import ../resources/identity.nix;
in
{
  imports = [ ../../packages/nixpkgs-issues-163080.nix ];

  config = {
    users.mutableUsers = false;

    users.groups.${identity.username}.gid = 1000;
    users.users.${identity.username} = {
      isNormalUser = true;
      uid = 1000;
      group = identity.username;
      extraGroups = [ "wheel" ];
      description = identity.name.short;
      hashedPassword = readFile ../local/resources/${identity.username}.passwd;
      shell = pkgs.zsh;
      openssh.authorizedKeys.keys = [ identity.ssh ];
    };
  };
}
