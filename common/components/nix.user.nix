{ lib, pkgs, ... }:

{
  imports = [ ../../packages/nixpkgs-issue-55674.nix ];

  config = {
    # Diff after rebuild
    home.activation.diff = lib.hm.dag.entryAnywhere ''
      ${pkgs.nvd}/bin/nvd diff "$oldGenPath" "$newGenPath"
    '';

    # Custom packages
    nixpkgs.overlays = [ (import ../packages.nix) ];
  };
}
