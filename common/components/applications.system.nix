{
  environment.localBinInPath = true;

  programs.trippy.enable = true;

  programs.zsh.enable = true;
}
