Contents:

  - [NixOS] configuration based on [`common/system.nix`](./common/system.nix)
  - [Home Manager] configuration based on [`common/user.nix`](./common/user.nix)
  - personal [package collection][nixpkgs] overlaid by [`common/packages.nix`](./common/packages.nix)


[Home Manager]: https://github.com/nix-community/home-manager/
[NixOS]: https://nixos.org/
[nixpkgs]: https://nixos.org/manual/nixpkgs/stable/
